Prerequisites
-----

I assume you have installed Docker and it is running.

See the [Docker website](http://www.docker.io/gettingstarted/#h_installation) for installation instructions.

Run Project with Docker
-----

1. Clone this repo

        git clone https://gitlab.com/petersabatini/task-scheduler.git

2. Build the image

        cd ..
        docker build -t task-scheduler .

    This will take a few minutes.

3. Run the image

        docker run -d -p 80:80 task-scheduler

4. Validate that the image is running

        open http://localhost/

Run Project with Node / NVM
-----

1. Clone this repo

        git clone https://gitlab.com/petersabatini/task-scheduler.git

2. This project uses Node 20. You can install `nvm` to manage multiple versions of node

        cd ..
        nvm use 20
        npm install
        npm run dev

4. Validate that the project is running

        open http://localhost:5173/