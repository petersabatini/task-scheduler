import { useState, useEffect, useRef } from "react";

import { DataTable } from 'primereact/datatable';
import { Button } from "primereact/button";
import { Column } from 'primereact/column';
import { Sidebar } from "primereact/sidebar";
import { Toast } from "primereact/toast";

import TaskService from "./services/TaskService";

import './App.css'
import Task from "./types/task";
import TaskForm from "./TaskForm";

import moment from "moment";
import cronstrue from "cronstrue";

function App() {
  const [tasks, setTasks] = useState<Task[]>([]);
  const [runs, setRuns] = useState<Task[]>([]);
  const [selectedTask, setSelectedTask] = useState<any>();
  const [visibleRight, setVisibleRight] = useState<boolean>(false);

  const toast = useRef<Toast>(null);

  const renderComponent = async() => {
    const tasks: Task[] = await TaskService.getAll();
    setTasks(tasks);

    const runs: Task[] = await TaskService.getRuns();
    setRuns(runs);
  }

  const onTaskAdd = () => {
    setSelectedTask({
      task_id: null,
      name: null,
      type: '',
      action: '',
      runtime: null,
      schedule: null,
      params: {},
    });
    setVisibleRight(true);
  };

  const onRowSelect = () => {
    setVisibleRight(true);
  };
  
  const onRowUnselect = () => {
    setVisibleRight(false);
  };

  const saveTask = async (value: any) => {
    await TaskService.create({
      ...value,
      runtime: value.runtime ? moment(value.runtime).format("YYYY-MM-DD HH:mm:ss") : null,
      params: {
        type: value.notification_type,
        to: value.notification_to,
        subject: value.notification_subject,
        body: value.notification_body,
      }
    });
    setVisibleRight(false);
    toast?.current?.show({ severity: 'info', summary: 'Info', detail: 'Task Created!' });
    renderComponent();
  }

  const deleteTask = async (taskId: string) => {
    await TaskService.remove(taskId);
    setVisibleRight(false);
    toast?.current?.show({ severity: 'info', summary: 'Info', detail: 'Task Deleted!' });
    renderComponent();
  }

  const formatDate = (value: any) => {
    return moment(value.run_date).format('LLLL');
  };

  const formatRecurringInterval = (value: any) => {
    return value.schedule ? cronstrue.toString(value.schedule) : moment(value.runtime).format('LLLL');
  };

  useEffect(() => {
    renderComponent();
  }, []);

  return (
    <>
      <div style={{ display: 'flex' }}>
        <h1 style={{ flex: 1 }}>Simple Task Scheduler</h1>
        <div>
          <Button icon="pi pi-plus" label="Add Task" onClick={() => onTaskAdd()} />
        </div>
      </div>

      <h2>Tasks</h2>
      <DataTable
        value={tasks}
        selectionMode="single"
        selection={selectedTask}
        onSelectionChange={(e) => setSelectedTask(e.value)}
        dataKey="task_id"
        onRowSelect={onRowSelect}
        onRowUnselect={onRowUnselect}
        metaKeySelection={false}
        tableStyle={{ minWidth: '50rem' }}
      >
        <Column field="name" header="Name"></Column>
        <Column field="type" header="Type"></Column>
        <Column field="schedule" header="Schedule" body={formatRecurringInterval}></Column>
        <Column field="create_date" header="Create Date" body={formatDate}></Column>
      </DataTable>

      <h2>Runs</h2>
      <DataTable
        value={runs}
        dataKey="task_id"
        tableStyle={{ minWidth: '50rem' }}
      >
        <Column field="run_date" header="Run Date" body={formatDate}></Column>
        <Column field="name" header="Task Name"></Column>
        <Column field="action" header="Task Action"></Column>
      </DataTable>

      <Toast ref={toast} />
      
      <Sidebar
        visible={visibleRight}
        header={selectedTask?.task_id ? 'Edit Task' : 'Create New Task'}
        position="right"
        onHide={() => setVisibleRight(false)}
        style={{ width: 800 }}
      >
        <TaskForm task={selectedTask} saveTask={saveTask} deleteTask={deleteTask} />
      </Sidebar>
    </>
  )
}

export default App
