import { useState, useEffect } from "react";

import { Button } from "primereact/button";
import { Dropdown } from "primereact/dropdown";
import { InputText } from "primereact/inputtext";
import { InputTextarea } from "primereact/inputtextarea";
import { Cron } from "react-js-cron";

import './App.css'
import 'react-js-cron/dist/styles.css';
import { Calendar } from "primereact/calendar";
import moment from "moment";

function TaskForm(props: any) {
  const { task, saveTask, deleteTask } = props;

  const [selectedTask, setSelectedTask] = useState<any>();

  const onChange = (property: string, value: any) => {
    const obj = { ...selectedTask };
    obj[property] = value;

    setSelectedTask(obj);
  };

  useEffect(() => {
    setSelectedTask({
      ...task,
      runtime: task?.runtime ? moment(task?.runtime).toDate() : null,
      notification_type: task?.params?.type,
      notification_to: task?.params?.to,
      notification_subject: task?.params?.subject,
      notification_body: task?.params?.body,
    });
  }, []);

  return (
    <>
      <div className="flex flex-wrap gap-3 mb-3">
        <div className="flex-auto">
          <label className="font-bold block mb-2">Name</label>
          <InputText
            value={selectedTask?.name}
            onChange={(e) => onChange('name', e.target.value)}
            className="w-full"
            placeholder="A Unique Task Name ..."
          />
        </div>
      </div>

      <div className="flex flex-wrap gap-3 mb-3">
        <div className="flex-auto">
          <label className="font-bold block mb-2">Task Type</label>
          <Dropdown
            value={selectedTask?.type}
            onChange={(e) => onChange('type', e.value)}
            options={[
              { name: "One Time", value: 'one-time' },
              { name: "Recurring", value: 'recurring' },
            ]}
            optionLabel="name" 
            placeholder="Type"
            className="w-full" 
          />
        </div>
      </div>

      { 
        selectedTask?.type === 'recurring' && (
          <div className="flex flex-wrap gap-3 mb-3">
            <div className="flex-auto">
              <label className="font-bold block mb-2">Schedule</label>
              <Cron
                value={selectedTask?.schedule}
                setValue={(e: any) => onChange('schedule', e)}
              />
            </div>
          </div>
        )
      }

      { 
        selectedTask?.type === 'one-time' && (
          <div className="flex flex-wrap gap-3 mb-3">
            <div className="flex-auto">
              <label className="font-bold block mb-2">Day-Time</label>
              <Calendar
                value={selectedTask?.runtime}
                onChange={(e) => onChange('runtime', e.value)}
                showTime
                hourFormat="12" 
                className="w-full" 
                placeholder="Day/Time of the Task"
              />
            </div>
          </div>
        )
      }

      <div className="flex flex-wrap gap-3 mb-3">
        <div className="flex-auto">
          <label className="font-bold block mb-2">Action</label>
          <Dropdown
            value={selectedTask?.action}
            onChange={(e) => onChange('action', e.value)}
            options={[
              { name: "Notification", value: 'notification' },
              { name: "Execute Backend Procedure", value: 'procedure' },
            ]}
            optionLabel="name" 
            placeholder="Action"
            className="w-full" 
          />
        </div>
      </div>

      { 
        selectedTask?.action === 'notification' && (
          <>
            <div className="flex flex-wrap gap-3 mb-3">
              <div className="flex-auto">
                <label className="font-bold block mb-2">Notification Type</label>
                <Dropdown
                  value={selectedTask?.notification_type}
                  onChange={(e) => onChange('notification_type', e.value)}
                  options={[
                    { name: "Email", value: 'email' },
                    { name: "Text / SMS", value: 'sms' },
                  ]}
                  optionLabel="name" 
                  placeholder="Notification Type"
                  className="w-full" 
                />
              </div>
            </div>

            <div className="flex flex-wrap gap-3 mb-3">
              <div className="flex-auto">
                <label className="font-bold block mb-2">To</label>
                <InputText
                  value={selectedTask?.notification_to}
                  onChange={(e) => onChange('notification_to', e.target.value)}
                  className="w-full"
                />
              </div>
            </div>

            <div className="flex flex-wrap gap-3 mb-3">
              <div className="flex-auto">
                <label className="font-bold block mb-2">Subject</label>
                <InputText
                  value={selectedTask?.notification_subject}
                  onChange={(e) => onChange('notification_subject', e.target.value)}
                  className="w-full"
                />
              </div>
            </div>

            <div className="flex flex-wrap gap-3 mb-3">
              <div className="flex-auto">
                <label className="font-bold block mb-2">Body</label>
                <InputTextarea
                  value={selectedTask?.notification_body}
                  onChange={(e) => onChange('notification_body', e.target.value)}
                  className="w-full"
                />
              </div>
            </div>
          </>
        )
      }

      <div style={{ display: 'flex'}}>
        <Button
          label={selectedTask?.task_id ? 'Save Changes' : 'Add Task'}
          icon="pi pi-check"
          className="w-full"
          onClick={() => saveTask(selectedTask)}
        />

        {
          selectedTask?.task_id && (
            <Button
              label="Delete Task"
              className="w-full"
              severity="danger"
              onClick={() => deleteTask(selectedTask.task_id)}
              style={{ marginLeft: 10 }}
            />
          )
        }
      </div>
    </>
  )
}3

export default TaskForm
