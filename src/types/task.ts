export default interface Task {
  task_id: string | null,
  name: string | null,
  type: string | null,
  action: string | null,
  schedule: string | null,
  params: any | null,
}
