import axios from "axios";
import Task from "../types/task";

const BASE_URL = 'http://localhost:3001';

export default class TaskService {
  static async getAll() {
    const { data } = await axios.get<Task[]>(`${BASE_URL}/tasks`);
    return data;
  };
  
  static get(id: any) {
    return axios.get<Task>(`${BASE_URL}/tasks/${id}`);
  };
  
  static create(data: Task) {
    return axios.post<Task>(`${BASE_URL}/tasks`, data);
  };
  
  static update(id: any, data: Task) {
    return axios.put<any>(`${BASE_URL}/tasks/${id}`, data);
  };
  
  static remove(id: any) {
    return axios.delete<any>(`${BASE_URL}/tasks/${id}`);
  };
  
  static async getRuns() {
    const { data } = await axios.get<Task[]>(`${BASE_URL}/runs`);
    return data;
  };
}
